package main

import (
	"fmt"
	"net/http"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	URL := "http://google.com"

	r, _ := hhttp.NewClient().SetOptions(&hhttp.Options{CookieJar: true}).Get(URL).Do()

	r.SetCookie(URL, []*http.Cookie{{Name: "root", Value: "cookies"}})

	r, _ = r.Client.Get(URL).Do()
	r.Debug()

	fmt.Println(r.GetCookies(URL)) // request url cookies
	fmt.Println(r.Cookies)
}
