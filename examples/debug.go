package main

import (
	"fmt"
	"log"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	r, err := hhttp.NewClient().Get("https://httpbingo.org/get").Do()
	if err != nil {
		log.Fatal(err)
	}

	r.Debug()     // without body
	r.Debug(true) // with body

	fmt.Println(r.Time)
}
