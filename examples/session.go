package main

import (
	"fmt"
	"log"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	URL := "https://httpbingo.org/cookies"

	// need set options 'CookieJar=true' for session support with new cookiejar
	options := hhttp.Options{CookieJar: true}

	r, err := hhttp.NewClient().SetOptions(&options).Get(URL + "/set?name1=value1&name2=value2").Do()
	if err != nil {
		log.Fatal(err)
	}

	// check cookies
	log.Println(r.GetCookies(URL))

	// second request, returns cookie data
	r, _ = r.Client.Get(URL).Do()

	// check if cookies in response {"name1":"value1","name2":"value2"}
	fmt.Println(r.Body)
}
