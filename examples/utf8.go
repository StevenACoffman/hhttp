package main

import (
	"fmt"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	r, _ := hhttp.NewClient().Get("https://httpbingo.org/encoding/utf8").Do()
	fmt.Println(r.Body)
}
