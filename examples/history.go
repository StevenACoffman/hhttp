package main

import (
	"fmt"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	r, _ := hhttp.NewClient().SetOptions(&hhttp.Options{History: true}).Get("http://google.com").Do()

	fmt.Println(r.History.Referers())
	fmt.Println(r.History.StatusCodes())
	fmt.Println(r.History.Cookies())
	fmt.Println(r.History.URLS())
}
