package main

import (
	"fmt"
	"log"

	"gitlab.com/gitroot/hhttp"
)

func main() {
	r, err := hhttp.NewClient().SetOptions(&hhttp.Options{Timeout: 2}).Get("httpbingo.org/delay/3").Do()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(r.StatusCode)
}
