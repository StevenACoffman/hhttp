package hhttp

import (
	"time"
)

type Options struct {
	BasicAuth   interface{}
	CookieJar   bool
	History     bool
	MaxRedirect int
	Proxy       interface{}
	Timeout     time.Duration
	UserAgent   string
}
